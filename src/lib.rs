extern crate libguile_src as ffi;

pub mod scm;

pub use scm::SCM;
use std::os::raw::{c_char, c_void, c_int};
use std::ffi::CString;

pub fn boot_guile(inner_main: unsafe extern "C" fn(closure: *mut c_void, argc: c_int, argv: *mut *mut c_char)) {
    unsafe {
        ffi::scm_boot_guile(0, std::ptr::null_mut(), Some(inner_main), std::ptr::null_mut())
    }
}

pub fn primitive_load<S: Into<String>>(file: S) -> SCM {
    unsafe {
        let file = CString::new(file.into()).unwrap();
        ffi::scm_c_primitive_load(file.as_ptr()).into()
    }
}

pub fn define_subr0<S: Into<String>, R>(name: S, fnc: extern fn() -> R) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define_gsubr(name.as_ptr(), 0, 0, 0, fnc as *mut c_void).into()
    }
}

pub fn define_subr1<S: Into<String>, A, R>(name: S, fnc: extern fn(A) -> R) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define_gsubr(name.as_ptr(), 1, 0, 0, fnc as *mut c_void).into()
    }
}

pub fn define_subr2<S: Into<String>, A, B, R>(name: S, fnc: extern fn(A, B) -> R) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define_gsubr(name.as_ptr(), 2, 0, 0, fnc as *mut c_void).into()
    }
}

pub fn define_subr3<S: Into<String>, A, B, C, R>(name: S, fnc: extern fn(A, B, C) -> R) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define_gsubr(name.as_ptr(), 3, 0, 0, fnc as *mut c_void).into()
    }
}

pub fn define_subr4<S: Into<String>, A, B, C, D, R>(name: S, fnc: extern fn(A, B, C, D) -> R) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define_gsubr(name.as_ptr(), 4, 0, 0, fnc as *mut c_void).into()
    }
}

pub fn define_subr5<S: Into<String>, A, B, C, D, E, R>(name: S, fnc: extern fn(A, B, C, D, E) -> R) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define_gsubr(name.as_ptr(), 5, 0, 0, fnc as *mut c_void).into()
    }
}

pub fn define_subr6<S: Into<String>, A, B, C, D, E, F, R>(name: S, fnc: extern fn(A, B, C, D, E, F) -> R) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define_gsubr(name.as_ptr(), 6, 0, 0, fnc as *mut c_void).into()
    }
}

pub fn define<S: Into<String>>(name: S, value: SCM) -> SCM {
    unsafe {
        let name = CString::new(name.into()).unwrap();
        ffi::scm_c_define(name.as_ptr(), value.0).into()
    }
}

