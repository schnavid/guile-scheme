use crate::SCM;

impl SCM {
    pub fn make(class: SCM) -> SCM {
        unsafe {
            ffi::scm_make(class.0).into()
        }
    }
}

pub fn load_goops() {
    unsafe {
        ffi::scm_load_goops()
    }
}
