use crate::SCM;

impl From<i8> for SCM {
    fn from(val: i8) -> Self {
        unsafe {
            ffi::scm_from_int8(val).into()
        }
    }
}

impl From<i16> for SCM {
    fn from(val: i16) -> Self {
        unsafe {
            ffi::scm_from_int16(val).into()
        }
    }
}

impl From<i32> for SCM {
    fn from(val: i32) -> Self {
        unsafe {
            ffi::scm_from_int32(val).into()
        }
    }
}

impl From<i64> for SCM {
    fn from(val: i64) -> Self {
        unsafe {
            ffi::scm_from_int64(val).into()
        }
    }
}

impl From<u8> for SCM {
    fn from(val: u8) -> Self {
        unsafe {
            ffi::scm_from_uint8(val).into()
        }
    }
}

impl From<u16> for SCM {
    fn from(val: u16) -> Self {
        unsafe {
            ffi::scm_from_uint16(val).into()
        }
    }
}

impl From<u32> for SCM {
    fn from(val: u32) -> Self {
        unsafe {
            ffi::scm_from_uint32(val).into()
        }
    }
}

impl From<u64> for SCM {
    fn from(val: u64) -> Self {
        unsafe {
            ffi::scm_from_uint64(val).into()
        }
    }
}

impl Into<i8> for SCM {
    fn into(self) -> i8 {
        unsafe {
            ffi::scm_to_int8(self.0)
        }
    }
}

impl Into<i16> for SCM {
    fn into(self) -> i16 {
        unsafe {
            ffi::scm_to_int16(self.0)
        }
    }
}

impl Into<i32> for SCM {
    fn into(self) -> i32 {
        unsafe {
            ffi::scm_to_int32(self.0)
        }
    }
}

impl Into<i64> for SCM {
    fn into(self) -> i64 {
        unsafe {
            ffi::scm_to_int64(self.0)
        }
    }
}

impl Into<u8> for SCM {
    fn into(self) -> u8 {
        unsafe {
            ffi::scm_to_uint8(self.0)
        }
    }
}

impl Into<u16> for SCM {
    fn into(self) -> u16 {
        unsafe {
            ffi::scm_to_uint16(self.0)
        }
    }
}

impl Into<u32> for SCM {
    fn into(self) -> u32 {
        unsafe {
            ffi::scm_to_uint32(self.0)
        }
    }
}

impl Into<u64> for SCM {
    fn into(self) -> u64 {
        unsafe {
            ffi::scm_to_uint64(self.0)
        }
    }
}

impl SCM {
    pub fn is_integer(self) -> bool {
        unsafe {
            match ffi::scm_is_integer(self.0) {
                1 => true,
                _ => false
            }
        }
    }
}
