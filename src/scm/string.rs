use crate::SCM;

impl From<String> for SCM {
    fn from(val: String) -> Self {
        unsafe {
            ffi::scm_from_utf8_stringn(val.as_ptr() as *const i8, val.len()).into()
        }
    }
}

impl Into<String> for SCM {
    fn into(self) -> String {
        unsafe {
            let mut len = 0;
            let buf = ffi::scm_to_utf8_stringn(self.0, &mut len) as *mut u8;
            String::from_utf8_unchecked(Vec::from_raw_parts(buf, len, len))
        }
    }
}

impl SCM {
    pub fn is_string(self) -> bool {
        unsafe {
            SCM::from(ffi::scm_string_p(self.0)).is_true()
        }
    }
}
