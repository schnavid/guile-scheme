use crate::SCM;

impl SCM {
    pub fn make_list(n: u32, value: SCM) -> SCM {
        unsafe {
            ffi::scm_make_list(SCM::from(n).into(), value.into()).into()
        }
    }

    pub fn list_1(el1: SCM) -> SCM {
        unsafe {
            ffi::scm_list_1(el1.into()).into()
        }
    }

    pub fn list_set_x(self, idx: u32, val: SCM) -> SCM {
        unsafe {
            ffi::scm_list_set_x(self.0, SCM::from(idx).into(), val.into()).into()
        }
    }
}