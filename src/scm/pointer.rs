use std::any::Any;
use crate::SCM;
use std::os::raw::c_void;

impl SCM {
    pub fn from_any<T: Any + Sized>(any: T) -> SCM {
        unsafe {
            let ptr = Box::into_raw(Box::new(any)) as *mut c_void;

            ffi::scm_from_pointer(ptr, None).into()
        }
    }

    pub fn to_any<T: Any + Sized>(self) -> Box<T> {
        unsafe {
            let ptr = ffi::scm_to_pointer(self.0) as *mut T;

            Box::from_raw(ptr)
        }
    }
}