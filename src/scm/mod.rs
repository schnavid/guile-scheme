use std::ffi::CString;

pub mod from;
pub mod boolean;
pub mod integer;
pub mod string;
pub mod pointer;
pub mod list;
pub mod goops;

#[derive(Copy, Clone)]
pub struct SCM(pub(crate) ffi::SCM);

unsafe impl Send for SCM {}
unsafe impl Sync for SCM {}

impl std::fmt::Debug for SCM {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "SCM {{ .. }}")
    }
}

impl From<ffi::SCM> for SCM {
    fn from(val: ffi::SCM) -> Self {
        SCM(val)
    }
}

impl Into<ffi::SCM> for SCM {
    fn into(self) -> ffi::SCM {
        self.0
    }
}

impl SCM {
    pub fn variable_ref(self) -> SCM {
        unsafe {
            ffi::scm_variable_ref(self.0).into()
        }
    }

    pub fn lookup<S: Into<String>>(name: S) -> SCM {
        unsafe {
            let name = CString::new(name.into()).unwrap();
            ffi::scm_c_lookup(name.as_ptr()).into()
        }
    }

    pub fn call(self, args: &[SCM]) -> SCM {
        match args.len() {
            0 => unsafe { ffi::scm_call_0(self.0).into() },
            1 => unsafe { ffi::scm_call_1(self.0, args[0].0).into() },
            2 => unsafe { ffi::scm_call_2(self.0, args[0].0, args[1].0).into() },
            _ => todo!()
        }
    }
}
