use crate::SCM;

impl From<f32> for SCM {
    fn from(val: f32) -> Self {
        unsafe {
            ffi::scm_from_double(val as f64).into()
        }
    }
}

impl From<f64> for SCM {
    fn from(val: f64) -> Self {
        unsafe {
            ffi::scm_from_double(val).into()
        }
    }
}


