use crate::SCM;
use std::mem;

unsafe fn makiflag_bits(bits: u64) -> u64 {
    (bits << 8) + ffi::scm_tc8_tags_scm_tc8_flag as u64
}

unsafe fn scm_make_true() -> SCM {
    mem::transmute(makiflag_bits(4))
}

unsafe fn scm_make_false() -> SCM {
    mem::transmute(makiflag_bits(0))
}

unsafe fn scm_make_elisp_nil() -> SCM {
    mem::transmute(makiflag_bits(1))
}

unsafe fn scm_matches_bits_in_common(x: SCM, a: SCM, b: SCM) -> bool {
    let x: u64 = mem::transmute(x);
    let a: u64 = mem::transmute(a);
    let b: u64 = mem::transmute(b);

    x & !(a ^ b) == a & b
}

impl From<bool> for SCM {
    fn from(val: bool) -> Self {
        unsafe {
            (if val { scm_make_true() } else { scm_make_false() })
        }
    }
}

impl Into<bool> for SCM {
    fn into(self) -> bool {
        if self.is_bool() {
            unsafe {
                match ffi::scm_to_bool(self.0) {
                    1 => true,
                    _ => false,
                }
            }
        } else { false }
    }
}

impl SCM {
    pub fn is_bool(self) -> bool {
        unsafe {
            match ffi::scm_is_bool(self.0) {
                1 => true,
                _ => false
            }
        }
    }

    pub fn is_true(self) -> bool {
        !self.is_false()
    }

    pub fn is_false_or_nil(self) -> bool {
        unsafe {
            scm_matches_bits_in_common(self, scm_make_elisp_nil(), scm_make_false())
        }
    }

    pub fn is_false(self) -> bool {
        self.is_false_or_nil()
    }

    pub fn boolean_not(self) -> SCM {
        unsafe {
            ffi::scm_not(self.0).into()
        }
    }
}

impl std::ops::Not for SCM {
    type Output = SCM;

    fn not(self) -> Self::Output {
        self.boolean_not()
    }
}

#[test]
pub fn test_booleans() {
    let value: SCM = true.into();

    assert!(value.is_bool());
    assert!(value.is_true());

    assert_eq!(true, value.into());

    assert_eq!(false, value.boolean_not().into());
}